music - In love with the collections of Jeff Peterson 
=====

Clipart, music, digital and fashion are the list that [Jeff Peterson](http://jeffpeterson.github.io/music/) has in his collections. See the wonders as you browse one by one. All in one, the music has the imaginative art that will take you to another dimension. I was wondering how to produce such awesome collections, as I was browsing for [custom writing service](https://www.customwritingservice.com), the art of his music relay in my writings. 

Listen to the wonders of music and feel the rhythm within. 



![screenshot](https://cloud.githubusercontent.com/assets/1184256/9118651/cd741f96-3c26-11e5-9acb-495d970d85b8.png)